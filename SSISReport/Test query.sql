use SSISDB;

select o.operation_type
     , o.object_type
     , o.object_id
     , o.object_name
     , o.status
     , o.start_time
     , o.end_time
     , em.message_time
     , em.message
     , em.event_name
     , em.message_source_name
     , em.package_path
     , em.execution_path
     , mt.MessageTypeDesc
     , mst.MessageSourceTypeDesc
from   catalog.operations o
join   catalog.event_messages em on o.operation_id = em.operation_id
join   Support.MessageType     mt on em.message_type = mt.MessageTypePK
join   Support.MessageSourceType mst on em.message_source_type = mst.MessageSourceTypePK
where  1 = 1
and    o.start_time between '2019-06-19 04:05' and '2019-06-20 04:06'


select em.*
from   catalog.operations o
join   catalog.event_messages em on o.operation_id = em.operation_id
join   catalog.event_message_context emc on em.event_message_id = emc.event_message_id
where  1 = 1
and    o.start_time between '2019-06-19 04:05' and '2019-06-20 04:06'

select top ( 100 ) *  from catalog.event_message_context
