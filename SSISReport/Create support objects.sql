use SSISDB
go

create schema Support authorization dbo;
go

create table Support.ContextType
(
 ContextTypePK   smallint         not null,
 ContextTypeDesc nvarchar ( 100 )     null,
 constraint PK_ContextType primary key clustered ( ContextTypePK )
)
go

create table Support.EnvirnmentScope
(
 EnvirnmentScopePK   char ( 1 )       not null,
 EnvirnmentScopeDesc nvarchar ( 100 )     null,
 constraint PK_EnvirnmentScope primary key clustered ( EnvirnmentScopePK ) 
) 
go

create table Support.MessageSourceType
(
 MessageSourceTypePK   smallint         not null,
 MessageSourceTypeDesc nvarchar ( 100 )     null,
 constraint PK_MessageSourceType primary key clustered ( MessageSourceTypePK )
) 
go

create table Support.MessageType
(
 MessageTypePK   smallint         not null,
 MessageTypeDesc nvarchar ( 100 )     null,
 constraint PK_MessageType primary key clustered ( MessageTypePK )
) 
go

create table Support.ObjectType
(
 ObjectTypePK   smallint         not null,
 ObjectTypeDesc nvarchar ( 100 )     null,
 constraint PK_ObjectType primary key clustered ( ObjectTypePK )
) 
go

create table Support.OperationStatus
(
 OperationStatusPK   int              not null,
 OperationStatusDesc nvarchar ( 100 )     null,
 constraint PK_OperationStatus primary key clustered ( OperationStatusPK )
) 
go

create table Support.OperationType
(
 OperationTypePK   smallint         not null,
 OperationTypeDesc nvarchar ( 100 )     null,
 constraint PK_OperationType primary key clustered ( OperationTypePK )
) 
go

create table Support.ValidateType
(
 ValidateTypePK   char ( 1 )       not null,
 ValidateTypeDesc nvarchar ( 100 )     null,
 constraint PK_ValidateType primary key clustered ( ValidateTypePK )
) 
go

insert Support.ContextType ( ContextTypePK, ContextTypeDesc ) 
values ( 10, N'Task' )
     , ( 20, N'Pipeline' )
     , ( 30, N'Sequence' )
     , ( 40, N'For Loop' )
     , ( 50, N'Foreach Loop' )
     , ( 60, N'Package' )
     , ( 70, N'Variable' )
     , ( 80, N'Connection manager' )

insert Support.EnvirnmentScope ( EnvirnmentScopePK, EnvirnmentScopeDesc ) 
values ( N'A', N'All environment references associated with the project' )
     , ( N'D', N'No environment' )
     , ( N'S', N'Single environment' )

insert Support.MessageSourceType ( MessageSourceTypePK, MessageSourceTypeDesc ) 
values ( 10, N'Entry APIs, such as T-SQL and CLR Stored procedures' )
     , ( 20, N'External process used to run package (ISServerExec.exe)' )
     , ( 30, N'Package-level objects' )
     , ( 40, N'Control Flow tasks' )
     , ( 50, N'Control Flow containers' )
     , ( 60, N'Data Flow task' )

insert Support.MessageType ( MessageTypePK, MessageTypeDesc ) 
values ( -1, N'Unknown' )
     , ( 10, N'Pre-validate' )
     , ( 20, N'Post-validate' )
     , ( 30, N'Pre-execute' )
     , ( 40, N'Post-execute' )
     , ( 50, N'StatusChange' )
     , ( 60, N'Progress' )
     , ( 70, N'Information' )
     , ( 80, N'VariableValueChanged' )
     , ( 90, N'Diagnostic' )
     , ( 100, N'QueryCancel' )
     , ( 110, N'Warning' )
     , ( 120, N'Error' )
     , ( 130, N'TaskFailed' )
     , ( 140, N'DiagnosticEx' )
     , ( 200, N'Custom' )
     , ( 400, N'NonDiagnostic' )

insert Support.ObjectType ( ObjectTypePK, ObjectTypeDesc ) 
values ( 10, N'Folder' )
     , ( 20, N'Projet' )
     , ( 30, N'Package' )
     , ( 40, N'Environment' )
     , ( 50, N'Instance of Execution' )

insert Support.OperationStatus ( OperationStatusPK, OperationStatusDesc ) 
values ( 1, N'Created' )
     , ( 2, N'Running' )
     , ( 3, N'Canceled' )
     , ( 4, N'Failed' )
     , ( 5, N'Pending' )
     , ( 6, N'Ended Unexpectedly' )
     , ( 7, N'Succeeded' )
     , ( 8, N'Stopping' )
     , ( 9, N'Completed' )

insert Support.OperationType ( OperationTypePK, OperationTypeDesc ) 
values ( 1, N'Integration Services initialization' )
     , ( 2, N'Retention window (SQL Agent job)' )
     , ( 3, N'MaxProjectVersion (SQL Agent job)' )
     , ( 101, N'deploy_project (Stored procedure)' )
     , ( 106, N'restore_project (Stored procedure)' )
     , ( 200, N'create_execution and start_execution (Stored procedures)' )
     , ( 202, N'stop_operation (Stored procedure)' )
     , ( 300, N'validate_project (Stored procedure)' )
     , ( 301, N'validate_package (Stored procedure)' )
     , ( 1000, N'configure_catalog (Stored procedure)' )

insert Support.ValidateType ( ValidateTypePK, ValidateTypeDesc ) 
values ( N'D', N'Dependency validation' )
     , ( N'F', N'Full Validation' )
