﻿WITH Tasks
AS (
  SELECT session_id,
         wait_type,
         wait_duration_ms,
         blocking_session_id,
         resource_description,
         PageID = cast ( right ( resource_description,
                                 len ( resource_description ) -
                                 charindex ( ':', resource_description,
                                 3
                               )
                       ) as int )
  from   sys.dm_os_waiting_tasks
  where  wait_type like 'PAGE%LATCH_%'
  and    resource_description LIKE '2:%' 
)
select  session_id,
        wait_type,
        wait_duration_ms,
        blocking_session_id,
        resource_description,
        ResourceType = case
                         when PageID = 1 Or PageID % 8088 = 0
                           then 'Is PFS Page'
                         when PageID = 2 Or PageID % 511232 = 0
                           then 'Is GAM Page'
                         when PageID = 3 Or (PageID - 1) %  511232 = 0
                           then 'Is SGAM Page'
                         else 'Is Not PFS, GAM, or SGAM page'
                       end
from Tasks;

use tempdb

select FreePages           = sum ( unallocated_extent_page_count ),
       FreeSpaceMB         = sum ( unallocated_extent_page_count ) / 128.0,
       VersionStorePages   = sum ( version_store_reserved_page_count),
       VersionStoreMB      = sum ( version_store_reserved_page_count) / 128.0,
       InternalObjectPages = sum ( internal_object_reserved_page_count),
       InternalObjectsMB   = sum ( internal_object_reserved_page_count) / 128.0,
       UserObjectPages     = sum ( user_object_reserved_page_count),
       UserObjectsMB       = sum ( user_object_reserved_page_count) / 128.0
from   sys.dm_db_file_space_usage;
