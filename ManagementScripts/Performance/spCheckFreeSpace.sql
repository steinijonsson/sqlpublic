﻿create procedure spCheckFreeSpace ( @LowLimit decimal ( 15, 2 ) = 10.0 )
as
  declare @tab char ( 1 ) = char ( 9 )
  declare @ProfileName     sysname         = 'DefaultMail'
  declare @MailRecipients  varchar ( max ) = 'steini@miracle.is'
  declare @MailFromAddress varchar ( max ) = 'steini@miracle.is'

  ----------------------------------------------------------------------------------------------------
  -- Create temp table ( and drop it if it already exists ).
  ----------------------------------------------------------------------------------------------------
  if object_id ( 'tempdb..##UsedSpace' ) is not null
    drop table ##UsedSpace
  create table ##UsedSpace
  (
      [Database name]         sysname           not null
    , [File name]             sysname           not null
    , [Physical name]         nvarchar ( 260 )  not null
    , [Total Size in MB]      decimal ( 15, 2 ) not null
    , [Available Space In MB] decimal ( 15, 2 ) null
    , [Available Space in %]  decimal ( 15, 2 ) null
    , [Filegroup Name]        sysname           null
  )

  ----------------------------------------------------------------------------------------------------
  -- Populate temp table
  ----------------------------------------------------------------------------------------------------
  exec sp_MSforeachdb '
  if db_id ( "?" ) > 4
  begin
    use [?];
    print ''Checking database "?"'';
    with cte as
    (
      select          f.name                                                    [File Name] 
                    , f.physical_name                                           [Physical Name]
                    , cast ( ( f.size / 128.0 ) as decimal ( 15, 2 ) )          [Total Size in MB]
                    , cast ( f.size / 128.0 - 
                             cast ( fileproperty ( f.name, ''SpaceUsed'' ) as int 
                           ) / 128.0 as decimal ( 15, 2 ) )                     [Available Space In MB]
                    , [file_id]                                                 [file_id]
                    , fg.name                                                   [Filegroup Name]
      from            sys.database_files as f with (nolock) 
      left outer join sys.data_spaces    as fg with (nolock) 
      on              f.data_space_id = fg.data_space_id --option (recompile)
    )
    insert ##UsedSpace ( [Database name], [File name], [Physical name], [Total Size in MB], [Available Space In MB], [Available Space in %], [Filegroup Name] )
    select "?" [Database]
         , [File Name]
         , [Physical Name]
         , [Total Size in MB]
         , [Available Space in MB]
         , ( [Available Space in MB] / [Total Size in MB] * 100.0 ) [Available Space in %]
         , [Filegroup Name]
    from   cte
    option ( recompile )
  end'

  declare @RowCount int

  select   @RowCount = count(*)
  from     ##UsedSpace 
  where    [Available Space in %] < @LowLimit


  if @RowCount > 0
  begin
    declare @q varchar ( max )= 
     'set nocount on;
      select   [Database name], [File name], [Physical name], [Total Size in MB], [Available Space In MB], [Available Space in %], [Filegroup Name]
      from     ##UsedSpace 
      where    [Available Space in %] < ' + convert ( varchar, @LowLimit ) + '
      order by [Available Space in %]'


    print 'Sending email...'
    exec msdb..sp_send_dbmail
      @profile_name = @ProfileName
    , @recipients = @MailRecipients
    , @from_address = @MailFromAddress
    , @subject = 'Available free space is low'
    , @body = 'Please check available space for the following databases (see attachment).'
    --, @body_format = 'HTML'
    , @query = @q
    , @attach_query_result_as_file = 1
    , @query_attachment_filename = 'Space.csv'
    , @exclude_query_output = 1
    , @query_result_separator = @tab
    , @query_result_no_padding = 1
    , @query_result_width = 65535
    print 'Email sent.'
  end
  else
    print 'No email sent.'

  if object_id ( 'tempdb..##UsedSpace' ) is not null
    drop table ##UsedSpace
