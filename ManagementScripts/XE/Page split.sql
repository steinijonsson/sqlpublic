﻿if exists ( select 1 
            from   sys.server_event_sessions 
            where  name = 'Page split'
          )
  drop event session [Page split] on server

create event session [Page split] 
on server 

add event sqlserver.page_split
(
  action ( sqlserver.database_name
         , sqlserver.query_hash
         , sqlserver.query_plan_hash
         , sqlserver.sql_text
     )
)

add target package0.event_file 
( 
  set filename = N'D:\XE\PageSplit.xel' 
    , max_file_size= 512
    , max_rollover_files = 3
)
with ( max_memory = 16384 kb
     , event_retention_mode = allow_single_event_loss
     , max_dispatch_latency = 30 seconds
     , max_event_size = 0 kb
     , memory_partition_mode = none
     , track_causality = on
     , startup_state = on
   )
