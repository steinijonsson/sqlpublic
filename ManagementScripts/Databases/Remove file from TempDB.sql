﻿----------------------------------------------------------------------------------------------------
-- List index fragmentations
----------------------------------------------------------------------------------------------------
use tempdb
dbcc dropcleanbuffers
dbcc freeproccache
dbcc freesessioncache
dbcc freesystemcache ( 'all' )
dbcc shrinkfile ( N'<Filename, sysname, >' , emptyfile)
alter database tempdb remove file [<Filename, sysname,>]
