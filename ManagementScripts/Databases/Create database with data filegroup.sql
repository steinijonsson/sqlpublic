﻿----------------------------------------------------------------------------------------------------
-- Create database template with primary filegroup for system objects and separated filegroups
-- for data and indexes.
----------------------------------------------------------------------------------------------------
use [master]
create database [<database_name, nvarchar(50), DB1>]
containment = none
on primary 
( 
  name = N'<database_name, nvarchar(50), DB1>_Primary', 
  filename = N'<data_directory, nvarchar ( 50 ), C:\Docs2\SQLData\Default>\<database_name, nvarchar(50), DB1>_Primary.mdf' , 
  size = 50 MB,
  maxsize = 1 GB, 
  filegrowth = 50 MB
)
log on 
( 
  name = N'<database_name, nvarchar(50), DB1>_Log', 
  filename = N'<log_directory, nvarchar ( 50 ),C:\Docs2\SQLTLogs\Default>\<database_name, nvarchar(50), DB1>_Log.ldf' , 
  size = 100 MB, 
  maxsize = 5 GB, 
  filegrowth = 100 MB 
)
go

alter database [<database_name, nvarchar(50), DB1>] set compatibility_level = 140
alter database [<database_name, nvarchar(50), DB1>] set recovery simple 
go

use [<database_name, nvarchar(50), DB1>]
go

alter database [<database_name, nvarchar(50), DB1>] add filegroup [Data1] 
go

alter database [<database_name, nvarchar(50), DB1>]
add file
( 
  name = N'<database_name, nvarchar(50), DB1>_Data1', 
  filename = N'<data_directory, nvarchar ( 50 ), C:\Docs2\SQLData\Default>\<database_name, nvarchar(50), DB1>_Data1.ndf' , 
  size = 100 MB, 
  maxsize = 5 GB, 
  filegrowth = 100 MB
)
to filegroup [Data1]

if not exists (select name from sys.filegroups where is_default=1 and name = N'Data1') 
alter database [<database_name, nvarchar(50), DB1>] modify filegroup [Data1] default
go

alter database [<database_name, nvarchar(50), DB1>] add filegroup [Index1] 
go

alter database [<database_name, nvarchar(50), DB1>]
add file
( 
  name = N'<database_name, nvarchar(50), DB1>_Index1', 
  filename = N'<index_directory, nvarchar ( 50 ), C:\Docs2\SQLData\Default>\<database_name, nvarchar(50), DB1>_Index1.ndf' , 
  size = 100 MB, 
  maxsize = 5 GB, 
  filegrowth = 100 MB
)
to filegroup [Index1]

exec sp_changedbowner @loginame = N'sa'
go

use [master]
