﻿select a.FileId                                                                                               [File id]
	   , convert ( decimal ( 12, 2 ),round ( a.size / 128.000, 2 ) )                                            [File size (MB)]
	   , convert ( decimal ( 12, 2 ),round ( fileproperty ( a.name, 'SpaceUsed' ) / 128.000, 2 ) )              [Space used (MB)]
	   , convert ( decimal ( 12, 2 ),round ( ( a.size - fileproperty ( a.name, 'SpaceUsed' ) ) / 128.000, 2 ) ) [Free space (MB)]
	   , a.name                                                                                                 [Name]
	   , a.filename                                                                                             [Filename]
from   dbo.sysfiles a
