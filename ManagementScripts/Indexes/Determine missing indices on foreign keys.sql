﻿with cteFK as
( 
  select     object_name ( fk.referenced_object_id ) pk_table ,
             c2.name                                 pk_column,
             kc.name                                 pk_index_name,
             object_name ( fk.parent_object_id )     fk_table ,
             c.name                                  fk_column ,
             fk.name                                 fk_name ,
             case 
               when i.object_id is not null 
                 then 1 
               else 0 
             end                                     does_fk_has_index ,
             i.is_primary_key                        is_fk_a_pk_also ,
             i.is_unique                             is_index_on_fk_unique ,
             fk.*
  from       sys.foreign_keys                   fk
  inner join sys.foreign_key_columns  fkc on  fkc.constraint_object_id = fk.object_id
  inner join sys.columns              c   on  c.object_id = fk.parent_object_id 
                                          and c.column_id = fkc.parent_column_id
  left  join sys.columns              c2  on  c2.object_id = fk.referenced_object_id 
                                          and c2.column_id = fkc.referenced_column_id
  left join  sys.key_constraints      kc  on  kc.parent_object_id = fk.referenced_object_id 
                                          and kc.type = 'PK'
  left join  sys.index_columns        ic  on  ic.object_id = c.object_id 
                                          and ic.column_id = c.column_id
  left join  sys.indexes              i   on  i.object_id = ic.object_id 
                                                and i.index_id = ic.index_id
)
select    * 
from      cteFK                     fk
left join sys.dm_db_partition_stats ps on  ps.object_id = fk.parent_object_id 
                                       and ps.index_id <= 1
where     does_fk_has_index = 0 -- and fk_table = 'LineItems'
order by  used_page_count desc
