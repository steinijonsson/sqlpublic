﻿----------------------------------------------------------------------------------------------------
-- List indexes for all tables in current database with size and filegroup.
----------------------------------------------------------------------------------------------------
with cte as 
(
  select s.name + '.' + o.[name] [Object name]
       , i.[name]                [Index name]
       , i.type_desc             [Index type]
       , f.[name]                [Filegroup name]
       , p.used_page_count * 8   [Index size in KB]
  from   sys.indexes                i
  join   sys.filegroups             f on  i.data_space_id = f.data_space_id
  join   sys.all_objects            o on  i.[object_id] = o.[object_id]
  join   sys.schemas                s on  o.schema_id = s.schema_id
  join   sys.dm_db_partition_stats  p on  i.object_id = p.object_id 
                                      and i.index_id = p.index_id
  where  i.data_space_id = f.data_space_id
  and    o.type = 'U'
)
select   [Object name]
       , [Index name]
       , [Index type]
       , [Filegroup name]
       , convert ( decimal ( 20, 2 )
                 , sum ( [Index size in KB] / 1024.0 ) 
                 ) [Index size in MB]
from     cte
group by [Object name]
       , [Index name]
       , [Index type]
       , [Filegroup name]
