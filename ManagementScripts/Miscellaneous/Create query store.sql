﻿alter database [<database_name, nvarchar(50)>] set query_store = on;
alter database [<database_name, nvarchar(50)>] set query_store clear;
alter database [<database_name, nvarchar(50)>] set query_store ( operation_mode = read_write );
