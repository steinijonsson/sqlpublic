﻿use msdb;

with cteLastJobExecution as
(
  select   max ( msdb.dbo.agent_datetime ( run_date, run_time ) ) [LastRunDate], job_id 
             from     msdb..sysjobhistory 
             where    step_id = 0
             group by job_id
)

select   j.name                               [Job name]
       , h.step_id                            [Step ID]
       , h.step_name                          [Step name]
       , timefromparts 
         ( ( run_duration / 10000 ) % 10000,
           ( run_duration / 100 ) % 100,
           ( run_duration % 100 ),
           0, 0
         )                                    [Duration]
from     msdb..sysjobs       j
join     msdb..sysjobhistory h  on  j.job_id = h.job_id
join     cteLastJobExecution lr on  j.job_id = lr.job_id
                                and msdb.dbo.agent_datetime ( h.run_date, h.run_time ) >= lr.LastRunDate
where    j.name in ( 'Stg - DW Axis'/*, 'DW - Process Motus SSAS'*/ )
order by j.name
       , h.step_id
go
